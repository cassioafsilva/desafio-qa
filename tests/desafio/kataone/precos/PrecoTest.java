package desafio.kataone.precos;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrecoTest {

	private CheckOut checkOut;

	@Before
	public void before() {
		Regras regras = new Regras();
		Desconto descontoProdutoA = new Desconto(3,  130);
		Desconto descontoProdutoB = new Desconto(2,  45);
		
		regras.criaRegra(new Produto("A",  50, descontoProdutoA));
		regras.criaRegra(new Produto("B",  30, descontoProdutoB));
		regras.criaRegra(new Produto("C",  20));
		regras.criaRegra(new Produto("D",  15));
		
		checkOut = new CheckOut(regras);
	}
	
	@Test
	public void tests_total() {
		assertEquals( 0, checkOut.getTotal());

		checkOut.scan("A");
		assertEquals( 50, checkOut.getTotal());

		checkOut.scan("B");
		assertEquals( 80, checkOut.getTotal());
		
		checkOut.scan("A");
		assertEquals( 130, checkOut.getTotal());
		
		checkOut.scan("A");
		assertEquals( 160, checkOut.getTotal());

		checkOut.scan("B");
		assertEquals( 175, checkOut.getTotal());
		
	}
	
	
	@Test
	public void testVazio() {
		
		assertEquals( 0, checkOut.preco(""));
		
	}
	
	@Test
	public void testA() {
		
		assertEquals(50, checkOut.preco("A"));
	}
	@Test
	public void testAB() {
		
		assertEquals(80, checkOut.preco("AB"));
	}
	
	@Test
	public void testCDBA() {
		
		assertEquals( 115, checkOut.preco("CDBA"));
		
	}
	
	@Test
	public void testAA() {
		
		
		assertEquals(100, checkOut.preco("AA"));
	}
	@Test
	public void testAAA() {
		
		
		assertEquals(130, checkOut.preco("AAA"));
		
	}
	
	@Test
	public void testAAAA() {
		
		
		assertEquals(180, checkOut.preco("AAAA"));
		
	}
	
	@Test
	public void testAAAAA() {
		
		
		assertEquals(230, checkOut.preco("AAAAA"));
		
	}

	@Test
	public void testAAAAAA() {
		
		
		assertEquals(260, checkOut.preco("AAAAAA"));
		
	}
	
	@Test
	public void testAAAB() {
		
		assertEquals(160, checkOut.preco("AAAB"));
		
	}
	@Test
	public void testAAABB() {
		
		assertEquals(175, checkOut.preco("AAABB"));
		
	}
	
	
	@Test
	public void testAAABBD() {
		
		assertEquals(190, checkOut.preco("AAABBD"));
		
	}
	
	@Test
	public void testDABABA() {
		
		assertEquals(190, checkOut.preco("DABABA"));
		
	}
	
	@Test
	public void test_incremental() {
		
		assertEquals( 0, checkOut.preco(""));
		
		assertEquals(50, checkOut.preco("A"));
		
		assertEquals(80, checkOut.preco("AB"));

		assertEquals( 115, checkOut.preco("CDBA"));
		
		assertEquals(100, checkOut.preco("AA"));
		
		assertEquals(130, checkOut.preco("AAA"));
		
		assertEquals(180, checkOut.preco("AAAA"));
		
		assertEquals(230, checkOut.preco("AAAAA"));
		
		assertEquals(260, checkOut.preco("AAAAAA"));
		
		assertEquals(160, checkOut.preco("AAAB"));
		
		assertEquals(175, checkOut.preco("AAABB"));
		
		assertEquals(190, checkOut.preco("AAABBD"));
		
		assertEquals(190, checkOut.preco("DABABA"));
		
	}
	

}
