#language: pt
Funcionalidade: Criação de Grupos no Whats App
	Como um Usuario do Whatss App
	Eu quero criar um grupo 
	De modo que eles participem do grupo
	
	Cenário: CriarGrupo
		Dado Que eu esteja na tela principal do Whats App
		Quando Clicar no icone de opções
		E Selecionar a opção "Novo grupo"
		E Selecionar meus amigos
		E Clicar em próxima etapa
		E Definir o nome do novo grupo como "Amigos"
		E Clicar em confirmar
		Então Deve redirecionar para o grupo criado
		E Deve possuir a mensagem "Você criou o grupo 'Amigos'"