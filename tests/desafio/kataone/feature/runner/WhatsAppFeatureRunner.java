package desafio.kataone.feature.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		 features = {"tests/desafio/kataone/feature/foto"})
		

public class WhatsAppFeatureRunner {
}