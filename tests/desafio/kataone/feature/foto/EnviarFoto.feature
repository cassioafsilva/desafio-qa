#language: pt
Funcionalidade: Envio de foto pelo whats app
	Como um Usuario do Whatss App
	Eu quero enviar uma foto para um determinado contato
	De modo que o esse contato receba tal foto
	
	Cenário: EnviarFoto
		Dado Que eu esteja na tela de conversa com um contato
		Quando Clicar no icone de tirar foto
		E Tira uma foto
		E Confirmar o envio
		Então Deve enviar a foto para o contato
		
		