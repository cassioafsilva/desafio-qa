package desafio.kataone.steps.grupo;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class CriarGrupoSteps {

	@Dado("^Que eu esteja na tela principal do Whats App$")
	public void que_eu_esteja_na_tela_principal_do_Whats_App() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Clicar no icone de opções$")
	public void clicar_no_icone_de_opções() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Selecionar a opção \"([^\"]*)\"$")
	public void selecionar_a_opção(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Selecionar meus amigos$")
	public void selecionar_meus_amigos() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Clicar em próxima etapa$")
	public void clicar_em_próxima_etapa() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Definir o nome do novo grupo como \"([^\"]*)\"$")
	public void definir_o_nome_do_novo_grupo_como(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Quando("^Clicar em confirmar$")
	public void clicar_em_confirmar() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Então("^Deve redirecionar para o grupo criado$")
	public void deve_redirecionar_para_o_grupo_criado() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Então("^Deve possuir a mensagem \"([^\"]*)\"$")
	public void deve_possuir_a_mensagem(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

}
