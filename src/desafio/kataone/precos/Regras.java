package desafio.kataone.precos;

import java.util.HashMap;
import java.util.Map;

public class Regras {
	
	private Map<String, Produto> precos;
	
	public Regras() {
		 precos = new HashMap<String, Produto>();
	}
	
	
	public void criaRegra(Produto produto){
		precos.put(produto.getNome(), produto);
	}
	
	public Produto getProduto(String good){
		return precos.get(good);
	}

}
