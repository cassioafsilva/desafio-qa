package desafio.kataone.precos;

public class Produto {

	private String nome;
	private int precoUnitario;
	private Desconto desconto;
	
	
	public Produto(String nome, int precoUnitario, Desconto desconto) {
		this.nome = nome;
		this.precoUnitario = precoUnitario;
		this.desconto = desconto;
	}
	
	public Produto(String nome, int precoUnitario) {
		this.nome = nome;
		this.precoUnitario = precoUnitario;
	}

	public String getNome() {
		return nome;
	}
	
	public int getPrecoUnitario() {
		return precoUnitario;
	}
	
	public Desconto getDesconto() {
		return desconto;
	}
	
	public boolean possuiDesconto(){
		
		return desconto != null;
	}
	
}
