package desafio.kataone.precos;

public class Desconto {

	private int qtdDesconto;
	private int precoEspecial;

	public Desconto(int qtd, int precoEspecial) {
		this.qtdDesconto = qtd;
		this.precoEspecial = precoEspecial;
	}
	
	public int getQtd() {
		return qtdDesconto;
	}
	
	public int getPrecoEspecial() {
		return precoEspecial;
	}

}
