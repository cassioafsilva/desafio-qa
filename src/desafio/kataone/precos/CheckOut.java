package desafio.kataone.precos;

import java.util.HashMap;
import java.util.Map;

public class CheckOut {

	private Regras regras;
	private int total;

	private Map<Character, Integer> itens;

	public CheckOut(Regras regras) {
		this.regras = regras;
		this.itens = new HashMap<Character, Integer>();
	}

	public int getTotal() {
		total = 0;

		for (Character itemGood : itens.keySet()) {
			
			total += calcularTotalPorItem(itemGood);
			
		}
		return total;
	}

	private int calcularTotalPorItem(Character itemGood) {
		int totalItem = 0;
		Produto produto = regras.getProduto(itemGood.toString());

		Integer qtdItems = itens.get(itemGood);
		int unitPrice = produto.getPrecoUnitario();
		
		if (produto.possuiDesconto()) {
			totalItem += calcularProdutoComDesconto(produto, qtdItems, unitPrice);
		}else {
			totalItem +=  qtdItems * unitPrice;
		}
		
		return totalItem;
	}

	private int calcularProdutoComDesconto(Produto produto, Integer qtdItems, int unitPrice) {
		int qtdDesconto = produto.getDesconto().getQtd();
		int precoEspecial = produto.getDesconto().getPrecoEspecial();
		int qtdItemsComDesconto = qtdItems / qtdDesconto;
		int valorItensComDesconto = qtdItemsComDesconto * precoEspecial;
		int itensSemDesconto = qtdItems % qtdDesconto;
		int valorItensSemDesconto = itensSemDesconto * unitPrice;
		
		return valorItensComDesconto + valorItensSemDesconto;
	}

	public void scan(String produto) {
		
		for (int i = 0; i < produto.length(); i++) {
			char p = produto.charAt(i);
			
			Integer qtdProduto = itens.get(p);
			
			if (qtdProduto == null) {
				itens.put(p, 1);
			} else {
				itens.put(p, qtdProduto + 1);
			}
		}

	}

	public int preco(String string) {
		itens.clear();
		scan(string);
		return getTotal();
	}

}
